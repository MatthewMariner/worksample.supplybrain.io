# Nathan James Full Stack Developer - Work Sample 1

A Laravel project which integrates a Client (**Client ABC**) REST API and fetches and stores 'Order' data into a MySQL database.

### Requirements <a id="requirements"></a>


This worksample requires data objects from our client, **Client ABC**, to be transformed into data records in our systems. **Order**s will be fetched via HTTP from **Client ABC**'s REST API, transformed into [Eloquent Models](https://laravel.com/docs/7.x/eloquent) and finally [seeded](https://laravel.com/docs/7.x/seeding) into a MySQL database ready to be served as a REST API.

#### Data

* ***Create*** a MySQL database with 3 tables: **Order**, **OrderItem** and **OrderLog**. An **Order** can have many **OrderItem**s, an **OrderItem** can have only 1 **Order**. An **Order** can have many **OrderLog**s, and **OrderLog** can have only 1 **Order**. 
* Refer to the [Order Schema], [OrderItem Schema] and [OrderLog Schema] specifications for data definitions.

* ***Create*** [Eloquent](https://laravel.com/docs/7.x/eloquent) Models to manage **Order**, **OrderItem** and **OrderLog**. Refer to the [Order Schema] and [Order Item Schema] specifications for ***fillable*** properties. 

* ***Unit Test group 1:*** [Database Testing](https://laravel.com/docs/7.x/database-testing) of the 3 Models CRUD and relational integrity.

#### Client

* ***Create*** a [Guzzle](http://docs.guzzlephp.org/en/stable/) HTTP Client which is capable of performing the HTTP Request-Response cycle referenced in [(A) Get 'New' Orders],  [(B) Get Order: 40721314] and [(C) Get Order: 91050439]. 

* ***Unit Test group 3***: HTTP Client, for example performing: [Get 'New' Orders] and [Get Order] calls to **Client ABC**.

#### API Resource

* ***Create*** a ****REST API** (Read/HTTP GET only) [Resources](https://laravel.com/docs/7.x/eloquent-resources) for the 3 tables. 
* Routes required are: `orders` (result is an array), `orders/{key}` (result is a object), `orderItems` (result is an array), `orderItems\{key}` (result is a object). 
* These API routes can filter results by Schema properties passed in the query string (ex. `externalKey={somevalue}` should filter the by the `externalKey` value).

* ***Unit Test group 2***: test cases for the 3 REST API endpoints/routes.

#### Integration Job

* ***Create*** the ***main job*** of the project as described in [Client Job], in a [Seeder](https://laravel.com/docs/7.x/seeding) `ClientABCSeeder` which will be called via command line using: `php artisan db:seed --class=ClientABCSeeder`.

* ***Create*** your own Transformation layer to convert the **Order** and the **OrderItem**(s) from the **Client ABC** JSON objects into Eloquent Model instances which can be served by Appendix [X], [Y] and [Z]. The submited work sample code is responsible for determining the mapping between systems by (1) inspecting the data values between **Client ABC** and the [expected results] (2) inspecting the [Schema] properties (ex. key/term, default, function, etc). *Use a common interface which accounts for different clients likely having different terms in their response objects properties. 

* ***Unit Test group 4***: Transformation layer. Use the example data in the [Appendix] for the input (API data) to output (Model data) use cases.

****Unit Test*** any of your own tests and cases you deem to be essential. For mock data, see the Appendix [A], [B], [C], [X], [Y], [Z]. 
*Reference ***Schemas*** to direct you in finding relevant and criticial cases of data validataion, data formatting and terminology.

This work sample is complete once all requirements are fulfilled and all test cases pass via `php artisan test`.

## HOW TO Submit

1. Clone this repository using your Gitlab account.
1. Create a new branch or fork (whatever name you choose).
1. Complete the [requirements] in this document.
1. Push a Merge Request to this repository.
1. Let us know your branch is ready for review (don't forget the name) via email or communication method you currently have with us.

## Scoring and Analysis

1. Overall Design and Implementation.
1. Attention to Detail and Semantics.
1. Problem Solving.
1. Transformation Layer.
1. Data Relations.
1. API Resource Implementation.
1. Best Practices.
1. Test Coverage and use cases.


### References

* [HTTP Client - Laravel Offical Documentation](https://laravel.com/docs/7.x/http-client)
* [HTTP Client: Guzzle Mock Testing](http://docs.guzzlephp.org/en/stable/testing.html)
* [Model: Eloquent](https://laravel.com/docs/7.x/eloquent)
* [Database: Testing](https://laravel.com/docs/7.x/database-testing)
* [Database: Seeding - Laravel Offical Documentation](https://laravel.com/docs/7.x/seeding)
* [Resources](https://laravel.com/docs/7.x/eloquent-resources)


# Appendix <a id="appendix"></a>

## Appendix A - Get 'New' Orders from Client ABC API <a id="appendix-a"></a>

### `GET /documents/order?statusList=New`

```text
GET /documents/order?statusList=New
Host: clientabc.example.com
```
### HTTP Response Body

<details>
  <summary>Click to  show/hide JSON.</summary>
   
```javascript
{
  "result": [
    {
      "documentDate": "12/22/2019",
      "cancelDate": "",
      "documentType": "850",
      "tagId": 0,
      "documentNumber": "90994127",
      "billOfLading": "",
      "salesChannel": "EDI",
      "customerOrderNumber": "",
      "transmitDate": "",
      "customerOrderRef": "",
      "vendor": "21416",
      "totalPallets": 0,
      "shipToLocation": "9179215296",
      "deliveryDate": "",
      "salesOrderNumber": "",
      "proNumber": "",
      "createDate": "2020-04-11 15:33:32.0",
      "carrierService": "",
      "trailerNumber": "",
      "externalId": "",
      "appointmentNumber": "",
      "faStatus": "",
      "shipDate": "12/24/2019",
      "documentAmount": "349.98",
      "partner": "NETSHOPS",
      "shipmentId": "",
      "sendCounter": 0,
      "consolidator": "",
      "departmentNumber": "",
      "carrierCode": "FDXSO",
      "totalWeight": "",
      "documentId": "40721314",
      "location": "",
      "shipToName": "Jon Snow",
      "releaseNumber": "",
      "poNumber": "90994127",
      "account": "NAT45DEMO",
      "status": "New",
      "detailCount": 1
    },
    {
      "documentDate": "01/16/2020",
      "cancelDate": "",
      "documentType": "850",
      "tagId": 0,
      "documentNumber": "91050439",
      "billOfLading": "",
      "salesChannel": "EDI",
      "customerOrderNumber": "",
      "transmitDate": "",
      "customerOrderRef": "",
      "vendor": "21416",
      "totalPallets": 0,
      "shipToLocation": "2243452532",
      "deliveryDate": "",
      "salesOrderNumber": "",
      "proNumber": "",
      "createDate": "2020-04-11 15:33:36.0",
      "carrierService": "",
      "trailerNumber": "",
      "externalId": "",
      "appointmentNumber": "",
      "faStatus": "",
      "shipDate": "01/17/2020",
      "documentAmount": "195.98",
      "partner": "NETSHOPS",
      "shipmentId": "",
      "sendCounter": 0,
      "consolidator": "",
      "departmentNumber": "",
      "carrierCode": "FDXES",
      "totalWeight": "",
      "documentId": "40721325",
      "location": "",
      "shipToName": "Ayra Stark",
      "releaseNumber": "",
      "poNumber": "91050439",
      "account": "NAT45DEMO",
      "status": "New",
      "detailCount": 1
    }
  ],
  "code": "OK",
  "message": "Total 2 document(s)"
}
```

</details>

## Appendix B - Get Client Order: 40721314 from Client ABC API <a id="appendix-b"></a>

### `GET /documents/order/40721314`

```text
GET /documents/order/40721314
Host: clientabc.example.com
```

### `HTTP/1.1 200 OK`

<details>
  <summary>Click to  show/hide JSON.</summary>

```javascript
{
    "extraDate": 0,
    "addresses": [
      {
        "country": "US",
        "address3": "",
        "address2": "c/o Hayneedle, 2118 Water Ridge Parkway",
        "city": "Charlotte",
        "contactEmail": "",
        "address1": "Walmart Inc.",
        "postalCode": "28217",
        "contactFax": "",
        "type": "BT",
        "companyName1": "Hayneedle Inc",
        "companyName2": "Hayneedle.com",
        "dunsType": "12",
        "recordId": "BT",
        "consolidator": "",
        "contactOther": "",
        "address4": "",
        "state": "NC",
        "contactPhone": "",
        "locationCode": "",
        "locationNumber": "18888804884",
        "dunsNumber": "18888804884"
      },
      {
        "country": "CA",
        "address3": "",
        "address2": "",
        "city": "Montreal",
        "contactEmail": "",
        "address1": "280 - 5050 Rue Pare",
        "postalCode": "H4P 1P3",
        "contactFax": "",
        "type": "RE",
        "companyName1": "Nathan James",
        "companyName2": "",
        "dunsType": "",
        "recordId": "RE",
        "consolidator": "",
        "contactOther": "",
        "address4": "",
        "state": "QC",
        "contactPhone": "",
        "locationCode": "",
        "locationNumber": "",
        "dunsNumber": ""
      },
      {
        "country": "CA",
        "address3": "",
        "address2": "",
        "city": "Montreal",
        "contactEmail": "",
        "address1": "280 - 5050 Rue Pare",
        "postalCode": "H4P 1P3",
        "contactFax": "",
        "type": "SF",
        "companyName1": "Nathan James",
        "companyName2": "",
        "dunsType": "",
        "recordId": "SF",
        "consolidator": "",
        "contactOther": "",
        "address4": "",
        "state": "QC",
        "contactPhone": "",
        "locationCode": "",
        "locationNumber": "",
        "dunsNumber": ""
      },
      {
        "country": "US",
        "address3": "",
        "address2": "",
        "city": "Winterfell",
        "contactEmail": "",
        "address1": "1234 Street",
        "postalCode": "12345",
        "contactFax": "",
        "type": "ST",
        "companyName1": "Jon Snow",
        "companyName2": "",
        "dunsType": "12",
        "recordId": "ST",
        "consolidator": "",
        "contactOther": "",
        "address4": "",
        "state": "NY",
        "contactPhone": "",
        "locationCode": "",
        "locationNumber": "9179215296",
        "dunsNumber": "9179215296"
      },
      {
        "country": "",
        "address3": "",
        "address2": "",
        "city": "",
        "contactEmail": "",
        "address1": "",
        "postalCode": "",
        "contactFax": "",
        "type": "VN",
        "companyName1": "",
        "companyName2": "",
        "dunsType": "",
        "recordId": "VN",
        "consolidator": "",
        "contactOther": "",
        "address4": "",
        "state": "",
        "contactPhone": "",
        "locationCode": "",
        "locationNumber": "",
        "dunsNumber": ""
      }
    ],
    "notes": [
      {
        "recordId": "001",
        "note": "Original Transmission of Order",
        "type": "BEG01"
      },
      {
        "recordId": "002",
        "note": "Direct to Consumer Order",
        "type": "BEG02"
      },
      {
        "recordId": "003",
        "note": "Order Contact: customer_service@hayneedle.com Telephone Number: 1-888-880-4884 E-Mail Address: customer_service@hayneedle.com",
        "type": "PER"
      },
      {
        "recordId": "004",
        "note": "Carrier: FDXSO FDX-Air-Standard Overnight",
        "type": "TD5"
      },
      {
        "recordId": "005",
        "note": "Supplier Message",
        "type": "N9"
      },
      {
        "recordId": "006",
        "note": "SM:For alternative ship methods please refer to the Hayneedle routing guide.",
        "type": "MTX"
      }
    ],
    "buyerEmail": "",
    "documentNumber": "90994127",
    "parcelRef01": "",
    "taxes": [],
    "type": "850",
    "parcelCarrier": "",
    "salesChannel": "EDI",
    "recordIdStr": "40721314",
    "customerOrderNumber": "",
    "parcelRef03": "",
    "parcelRef02": "",
    "recordId": 40721314,
    "vendorNumberAp": "",
    "itemXrefPrice": 0,
    "customerOrderRef": "",
    "terms": [],
    "parcelSignature": "",
    "poDate": 20191222,
    "orderCases": 14,
    "remitToAddress": {
      "country": "CA",
      "address3": "",
      "address2": "",
      "city": "Montreal",
      "contactEmail": "",
      "address1": "280 - 5050 Rue Pare",
      "postalCode": "H4P 1P3",
      "contactFax": "",
      "type": "RE",
      "companyName1": "Nathan James",
      "companyName2": "",
      "dunsType": "",
      "recordId": "RE",
      "consolidator": "",
      "contactOther": "",
      "address4": "",
      "state": "QC",
      "contactPhone": "",
      "locationCode": "",
      "locationNumber": "",
      "dunsNumber": ""
    },
    "carrierService": "",
    "salesChannelDiscount": 0,
    "shipAccountZip": "",
    "contractNumber": "",
    "receiveDate": "2020-04-11 15:33:32.0",
    "salesChannelShipping": 0,
    "shipDate": 20191224,
    "totalRoutResp": 0,
    "salesChannelHandling": 0,
    "detailLineCount": 2,
    "parcelResidential": "",
    "itemXrefCount": 0,
    "fobText": "",
    "shipFromAddress": {
      "country": "CA",
      "address3": "",
      "address2": "",
      "city": "Montreal",
      "contactEmail": "",
      "address1": "280 - 5050 Rue Pare",
      "postalCode": "H4P 1P3",
      "contactFax": "",
      "type": "SF",
      "companyName1": "Nathan James",
      "companyName2": "",
      "dunsType": "",
      "recordId": "SF",
      "consolidator": "",
      "contactOther": "",
      "address4": "",
      "state": "QC",
      "contactPhone": "",
      "locationCode": "",
      "locationNumber": "",
      "dunsNumber": ""
    },
    "carrierCode": "FDXSO",
    "departmentNumber": "",
    "parcelService": "",
    "poCategory": "",
    "requestedDeliveryDate": 0,
    "poType": "SA",
    "salesChannelTax": 0,
    "releaseNumber": "",
    "customerAccount": "",
    "items": [
      {
        "itemNotes": [
          {
            "recordId": "007",
            "note": "NETSHOPS Item Description: Nathan James Viktor 3 Piece Counter Height Dining Table Set Light Gray Dark Br",
            "type": "DTL"
          },
          {
            "recordId": "008",
            "note": "GROSS Weight Per Pack: 66 LB",
            "type": "DTL"
          }
        ],
        "packSize": 1,
        "itemReferenceEachesNo": 0,
        "cartonUom": "",
        "itemChargesAndAllowances": [],
        "qtyAsn": 0,
        "caseGroupCode": "",
        "itemDimensionWidth": 0,
        "originalLineNo": "1",
        "recordId": "0001",
        "lineNo": "0001",
        "skuNumber": "",
        "commodityCode": "",
        "mfgCity": "",
        "itemWidth": "",
        "qtyAck": 0,
        "unitPrice": 174.99,
        "custFld03Nam": "",
        "itemDimensionHeight": 0,
        "vendorItem": "41202",
        "itemReferencePackSize": 0,
        "unitSize": "",
        "shipDate": 0,
        "gtinNumber": "",
        "inners": 1,
        "upcCaseCode": "",
        "customerItem": "BLUY024-2",
        "cartonHeight": 0,
        "mfgCountry": "",
        "custFld03Val": "",
        "prepackCode": "",
        "custFld02Nam": "",
        "itemSubColor": "",
        "itemColor": "",
        "itemDimensionLength": 0,
        "itemReferencePrice": 0,
        "cartonWidth": 0,
        "qtyChange": 0,
        "qtyMultiCarton": 1,
        "unitSizeBuyer": "",
        "cartonLength": 0,
        "sellingPrice": 0,
        "custFld02Val": "",
        "expireDate": 0,
        "nrfColor": "",
        "priceBasis": "",
        "itemWeight": 66,
        "upcCode": "628110580375",
        "nrfDesc": "",
        "custFld01Nam": "",
        "discountPercent": 0,
        "itemColorBuyer": "",
        "buyerStyleNo": "",
        "ticketType": "",
        "lotNumber": "",
        "itemDesc": "Nathan James Viktor 3 Piece Counter Height Dining Table Set Light Gray Dark Br",
        "extItemRef": "",
        "unitMeasure": "EA",
        "itemPackCode": "",
        "itemDimensionUom": "",
        "itemReferenceId": 0,
        "qtyOrder": 2,
        "rebatePrice": 0,
        "custFld01Val": "",
        "nrfSize": "",
        "itemComponents": [],
        "countryOfOrigin": "",
        "itemTaxes": [],
        "mfgName": "",
        "qtyInvoice": 0
      },
      {
        "itemNotes": [],
        "packSize": 1,
        "itemReferenceEachesNo": 0,
        "cartonUom": "",
        "itemChargesAndAllowances": [],
        "qtyAsn": 0,
        "caseGroupCode": "",
        "itemDimensionWidth": 0,
        "originalLineNo": "2",
        "recordId": "0002",
        "lineNo": "0002",
        "skuNumber": "",
        "commodityCode": "",
        "mfgCity": "",
        "itemWidth": "",
        "qtyAck": 0,
        "unitPrice": 90.83,
        "custFld03Nam": "",
        "itemDimensionHeight": 0,
        "vendorItem": "32703",
        "itemReferencePackSize": 0,
        "unitSize": "",
        "shipDate": 0,
        "gtinNumber": "",
        "inners": 1,
        "upcCaseCode": "",
        "customerItem": "",
        "cartonHeight": 0,
        "mfgCountry": "",
        "custFld03Val": "",
        "prepackCode": "",
        "custFld02Nam": "",
        "itemSubColor": "",
        "itemColor": "",
        "itemDimensionLength": 0,
        "itemReferencePrice": 0,
        "cartonWidth": 0,
        "qtyChange": 0,
        "qtyMultiCarton": 1,
        "unitSizeBuyer": "",
        "cartonLength": 0,
        "sellingPrice": 0,
        "custFld02Val": "",
        "expireDate": 0,
        "nrfColor": "",
        "priceBasis": "PE",
        "itemWeight": 0,
        "upcCode": "",
        "nrfDesc": "",
        "custFld01Nam": "",
        "discountPercent": 0,
        "itemColorBuyer": "",
        "buyerStyleNo": "",
        "ticketType": "",
        "lotNumber": "",
        "itemDesc": "",
        "extItemRef": "",
        "unitMeasure": "EA",
        "itemPackCode": "",
        "itemDimensionUom": "",
        "itemReferenceId": 0,
        "qtyOrder": 12,
        "rebatePrice": 0,
        "custFld01Val": "",
        "nrfSize": "",
        "itemComponents": [],
        "countryOfOrigin": "",
        "itemTaxes": [],
        "mfgName": "",
        "qtyInvoice": 0
      }
    ],
    "merchandiseCode": "",
    "poNumber": "90994127",
    "documentDate": 20191222,
    "shipToAddress": {
        "country": "US",
        "address3": "",
        "address2": "",
        "city": "Winterfell",
        "contactEmail": "",
        "address1": "12345 Street",
        "postalCode": "12345",
        "contactFax": "",
        "type": "ST",
        "companyName1": "Jon Snow",
        "companyName2": "",
        "dunsType": "12",
        "recordId": "ST",
        "consolidator": "",
        "contactOther": "",
        "address4": "",
        "state": "NY",
        "contactPhone": "",
        "locationCode": "",
        "locationNumber": "9179215296",
        "dunsNumber": "9179215296"
    },
    "cancelDate": 0,
    "routingInstructions": "FDX-Air-Standard Overnight",
    "serviceCode": "",
    "tagId": 0,
    "transportTermsCode": "",
    "extraDateType": "",
    "orderStatus": 0,
    "salesChannelStatus": "",
    "departmentDescription": "",
    "orderWeight": 132,
    "billToAddress": {
      "country": "US",
      "address3": "",
      "address2": "c/o Hayneedle, 2118 Water Ridge Parkway",
      "city": "Charlotte",
      "contactEmail": "",
      "address1": "Walmart Inc.",
      "postalCode": "28217",
      "contactFax": "",
      "type": "BT",
      "companyName1": "Hayneedle Inc",
      "companyName2": "Hayneedle.com",
      "dunsType": "12",
      "recordId": "BT",
      "consolidator": "",
      "contactOther": "",
      "address4": "",
      "state": "NC",
      "contactPhone": "",
      "locationCode": "",
      "locationNumber": "18888804884",
      "dunsNumber": "18888804884"
    },
    "salesChannelId": "",
    "totalRoutReq": 0,
    "shipMode": "",
    "vendor": "21416",
    "salesOrderNumber": "",
    "fobLocation": "",
    "isOpenShipment": 0,
    "transportCode": "",
    "transactionPurpose": "00",
    "externalId": "",
    "isValidateErrors": 0,
    "fobCode": "",
    "chargesAndAllowances": [],
    "documentAmount": 349.98,
    "isOpenAck": 0,
    "shipAccountNo": "",
    "ediBatchSize": "",
    "partner": "NETSHOPS",
    "salesChannelTotal": 0,
    "buyerAccount": "",
    "isOpenInvoice": 0,
    "fulfillAuthStatus": "NOT_USED",
    "documentId": 40721314,
    "location": "",
    "currencyCode": "",
    "dupSourceId": 0,
    "account": "NAT45DEMO",
    "totalChanges": 0,
    "transportTermsQual": ""
}
```
</details>

## Appendix C - Get Client Order: 40721325 from Client ABC API <a id="appendix-c"></a>

### `GET documents/order/40721325`

```text
GET documents/order/40721325
Host: clientabc.example.com
```

### `HTTP/1.1 200 OK`
<details>
  <summary>Click to  show/hide JSON.</summary>
  
```javascript
{
  "extraDate": 0,
  "addresses": [
    {
      "country": "US",
      "address3": "",
      "address2": "c/o Hayneedle, 2118 Water Ridge Parkway",
      "city": "Charlotte",
      "contactEmail": "",
      "address1": "Walmart Inc.",
      "postalCode": "28217",
      "contactFax": "",
      "type": "BT",
      "companyName1": "Hayneedle Inc",
      "companyName2": "Hayneedle.com",
      "dunsType": "12",
      "recordId": "BT",
      "consolidator": "",
      "contactOther": "",
      "address4": "",
      "state": "NC",
      "contactPhone": "",
      "locationCode": "",
      "locationNumber": "18888804884",
      "dunsNumber": "18888804884"
    },
    {
      "country": "CA",
      "address3": "",
      "address2": "",
      "city": "Montreal",
      "contactEmail": "",
      "address1": "280 - 5050 Rue Pare",
      "postalCode": "H4P 1P3",
      "contactFax": "",
      "type": "RE",
      "companyName1": "Nathan James",
      "companyName2": "",
      "dunsType": "",
      "recordId": "RE",
      "consolidator": "",
      "contactOther": "",
      "address4": "",
      "state": "QC",
      "contactPhone": "",
      "locationCode": "",
      "locationNumber": "",
      "dunsNumber": ""
    },
    {
      "country": "CA",
      "address3": "",
      "address2": "",
      "city": "Montreal",
      "contactEmail": "",
      "address1": "280 - 5050 Rue Pare",
      "postalCode": "H4P 1P3",
      "contactFax": "",
      "type": "SF",
      "companyName1": "Nathan James",
      "companyName2": "",
      "dunsType": "",
      "recordId": "SF",
      "consolidator": "",
      "contactOther": "",
      "address4": "",
      "state": "QC",
      "contactPhone": "",
      "locationCode": "",
      "locationNumber": "",
      "dunsNumber": ""
    },
    {
      "country": "US",
      "address3": "",
      "address2": "STE 302",
      "city": "ARLINGTON HEIGHTS",
      "contactEmail": "",
      "address1": "3233 N ARLINGTON HEIGHTS RD",
      "postalCode": "60004-1580",
      "contactFax": "",
      "type": "ST",
      "companyName1": "Ayra Stark",
      "companyName2": "Whole Health Family Medicine",
      "dunsType": "12",
      "recordId": "ST",
      "consolidator": "",
      "contactOther": "",
      "address4": "",
      "state": "IL",
      "contactPhone": "",
      "locationCode": "",
      "locationNumber": "2243452532",
      "dunsNumber": "2243452532"
    },
    {
      "country": "",
      "address3": "",
      "address2": "",
      "city": "",
      "contactEmail": "",
      "address1": "",
      "postalCode": "",
      "contactFax": "",
      "type": "VN",
      "companyName1": "",
      "companyName2": "",
      "dunsType": "",
      "recordId": "VN",
      "consolidator": "",
      "contactOther": "",
      "address4": "",
      "state": "",
      "contactPhone": "",
      "locationCode": "",
      "locationNumber": "",
      "dunsNumber": ""
    }
  ],
  "notes": [
    {
      "recordId": "001",
      "note": "Original Transmission of Order",
      "type": "BEG01"
    },
    {
      "recordId": "002",
      "note": "Direct to Consumer Order",
      "type": "BEG02"
    },
    {
      "recordId": "003",
      "note": "Order Contact: customer_service@hayneedle.com Telephone Number: 1-888-880-4884 E-Mail Address: customer_service@hayneedle.com",
      "type": "PER"
    },
    {
      "recordId": "004",
      "note": "Carrier: FDXES FDX-Air-Express Saver",
      "type": "TD5"
    },
    {
      "recordId": "005",
      "note": "Supplier Message",
      "type": "N9"
    },
    {
      "recordId": "006",
      "note": "SM:For alternative ship methods please refer to the Hayneedle routing guide.",
      "type": "MTX"
    }
  ],
  "buyerEmail": "",
  "documentNumber": "91050439",
  "parcelRef01": "",
  "taxes": [],
  "type": "850",
  "parcelCarrier": "",
  "salesChannel": "EDI",
  "recordIdStr": "40721325",
  "customerOrderNumber": "",
  "parcelRef03": "",
  "parcelRef02": "",
  "recordId": 40721325,
  "vendorNumberAp": "",
  "itemXrefPrice": 0,
  "customerOrderRef": "",
  "terms": [],
  "parcelSignature": "",
  "poDate": 20200116,
  "orderCases": 2,
  "remitToAddress": {
    "country": "CA",
    "address3": "",
    "address2": "",
    "city": "Montreal",
    "contactEmail": "",
    "address1": "280 - 5050 Rue Pare",
    "postalCode": "H4P 1P3",
    "contactFax": "",
    "type": "RE",
    "companyName1": "Nathan James",
    "companyName2": "",
    "dunsType": "",
    "recordId": "RE",
    "consolidator": "",
    "contactOther": "",
    "address4": "",
    "state": "QC",
    "contactPhone": "",
    "locationCode": "",
    "locationNumber": "",
    "dunsNumber": ""
  },
  "carrierService": "",
  "salesChannelDiscount": 0,
  "shipAccountZip": "",
  "contractNumber": "",
  "receiveDate": "2020-04-11 15:33:36.0",
  "salesChannelShipping": 0,
  "shipDate": 20200117,
  "totalRoutResp": 0,
  "salesChannelHandling": 0,
  "detailLineCount": 1,
  "parcelResidential": "",
  "itemXrefCount": 0,
  "fobText": "",
  "shipFromAddress": {
    "country": "CA",
    "address3": "",
    "address2": "",
    "city": "Montreal",
    "contactEmail": "",
    "address1": "280 - 5050 Rue Pare",
    "postalCode": "H4P 1P3",
    "contactFax": "",
    "type": "SF",
    "companyName1": "Nathan James",
    "companyName2": "",
    "dunsType": "",
    "recordId": "SF",
    "consolidator": "",
    "contactOther": "",
    "address4": "",
    "state": "QC",
    "contactPhone": "",
    "locationCode": "",
    "locationNumber": "",
    "dunsNumber": ""
  },
  "carrierCode": "FDXES",
  "departmentNumber": "",
  "parcelService": "",
  "poCategory": "",
  "requestedDeliveryDate": 0,
  "poType": "SA",
  "salesChannelTax": 0,
  "releaseNumber": "",
  "customerAccount": "",
  "items": [
    {
      "itemNotes": [
        {
          "recordId": "007",
          "note": "NETSHOPS Item Description: Nathan James Theo 5Tier Ladder Shelf Bookcase White",
          "type": "DTL"
        },
        {
          "recordId": "008",
          "note": "GROSS Weight Per Pack: 34 LB",
          "type": "DTL"
        }
      ],
      "packSize": 1,
      "itemReferenceEachesNo": 0,
      "cartonUom": "",
      "itemChargesAndAllowances": [],
      "qtyAsn": 0,
      "caseGroupCode": "",
      "itemDimensionWidth": 0,
      "originalLineNo": "1",
      "recordId": "0001",
      "lineNo": "0001",
      "skuNumber": "",
      "commodityCode": "",
      "mfgCity": "",
      "itemWidth": "",
      "qtyAck": 0,
      "unitPrice": 97.99,
      "custFld03Nam": "",
      "itemDimensionHeight": 0,
      "vendorItem": "65504",
      "itemReferencePackSize": 0,
      "unitSize": "",
      "shipDate": 0,
      "gtinNumber": "",
      "inners": 1,
      "upcCaseCode": "",
      "customerItem": "BLUY035-2",
      "cartonHeight": 0,
      "mfgCountry": "",
      "custFld03Val": "",
      "prepackCode": "",
      "custFld02Nam": "",
      "itemSubColor": "",
      "itemColor": "",
      "itemDimensionLength": 0,
      "itemReferencePrice": 0,
      "cartonWidth": 0,
      "qtyChange": 0,
      "qtyMultiCarton": 1,
      "unitSizeBuyer": "",
      "cartonLength": 0,
      "sellingPrice": 0,
      "custFld02Val": "",
      "expireDate": 0,
      "nrfColor": "",
      "priceBasis": "",
      "itemWeight": 34,
      "upcCode": "628110580900",
      "nrfDesc": "",
      "custFld01Nam": "",
      "discountPercent": 0,
      "itemColorBuyer": "",
      "buyerStyleNo": "",
      "ticketType": "",
      "lotNumber": "",
      "itemDesc": "Nathan James Theo 5Tier Ladder Shelf Bookcase White",
      "extItemRef": "",
      "unitMeasure": "EA",
      "itemPackCode": "",
      "itemDimensionUom": "",
      "itemReferenceId": 0,
      "qtyOrder": 2,
      "rebatePrice": 0,
      "custFld01Val": "",
      "nrfSize": "",
      "itemComponents": [],
      "countryOfOrigin": "",
      "itemTaxes": [],
      "mfgName": "",
      "qtyInvoice": 0
    },
    {
        "itemNotes": [],
        "packSize": 1,
        "itemReferenceEachesNo": 0,
        "cartonUom": "",
        "itemChargesAndAllowances": [],
        "qtyAsn": 0,
        "caseGroupCode": "",
        "itemDimensionWidth": 0,
        "originalLineNo": "2",
        "recordId": "0002",
        "lineNo": "0002",
        "skuNumber": "",
        "commodityCode": "",
        "mfgCity": "",
        "itemWidth": "",
        "qtyAck": 0,
        "unitPrice": 90.83,
        "custFld03Nam": "",
        "itemDimensionHeight": 0,
        "vendorItem": "32703",
        "itemReferencePackSize": 0,
        "unitSize": "",
        "shipDate": 0,
        "gtinNumber": "",
        "inners": 1,
        "upcCaseCode": "",
        "customerItem": "",
        "cartonHeight": 0,
        "mfgCountry": "",
        "custFld03Val": "",
        "prepackCode": "",
        "custFld02Nam": "",
        "itemSubColor": "",
        "itemColor": "",
        "itemDimensionLength": 0,
        "itemReferencePrice": 0,
        "cartonWidth": 0,
        "qtyChange": 0,
        "qtyMultiCarton": 1,
        "unitSizeBuyer": "",
        "cartonLength": 0,
        "sellingPrice": 0,
        "custFld02Val": "",
        "expireDate": 0,
        "nrfColor": "",
        "priceBasis": "PE",
        "itemWeight": 0,
        "upcCode": "",
        "nrfDesc": "",
        "custFld01Nam": "",
        "discountPercent": 0,
        "itemColorBuyer": "",
        "buyerStyleNo": "",
        "ticketType": "",
        "lotNumber": "",
        "itemDesc": "",
        "extItemRef": "",
        "unitMeasure": "EA",
        "itemPackCode": "",
        "itemDimensionUom": "",
        "itemReferenceId": 0,
        "qtyOrder": 12,
        "rebatePrice": 0,
        "custFld01Val": "",
        "nrfSize": "",
        "itemComponents": [],
        "countryOfOrigin": "",
        "itemTaxes": [],
        "mfgName": "",
        "qtyInvoice": 0
    }
  ]
```
 
</details>


## <a id="order-schema">Appendix SO - Order JSON Schema</a> 

<details>
  <summary>Click to  show/hide JSON.</summary>

```javascript
[ 
    {
        "@type": "hsd:DataProperty",
        "key": "salesChannel",
        "name": "Sales Channel",
        "range": "xsd:string"
    },  
    {
        "@type": "hsd:DataProperty",
        "key": "carrierCode",
        "name": "Retailer Carrier Code",
        "range": "xsd:string"
    },
    {
        "@type": "hsd:DataProperty",
        "key": "retailerCarrierServiceCode",
        "name": "Retailer Carrier Service Code",
        "range": "xsd:string"
    }, 
    {
        "@type": "hsd:DataProperty",
        "key": "deliveryLocationCode",
        "name": "Location Code",
        "range": "xsd:string"
    },
    {
        "@type": "hsd:DataProperty",
        "key": "dateCreated",
        "name": "Date Created",
        "range": "xsd:dateTime",
        "required": true
    },
    {
        "@type": "hsd:DataProperty",
        "key": "orderDate",
        "name": "Order Date",
        "range": "xsd:dateTime"
    },
    {
        "@type": "hsd:DataProperty",
        "key": "freightAmount",
        "name": "Freight Amount",
        "range": "xsd:float",
        "restrictions": {
            "maxLength" : 10
        }
    },
    {
        "@type": "hsd:DataProperty",
        "key": "customerPONumber",
        "name": "Customer PO Number",
        "range": "xsd:string",
        "restrictions": {
            "maxLength" : 20
        }
    },
    {
        "@type": "hsd:DataProperty",
        "key": "billingAddress1",
        "name": "Billing Address 1",
        "range": "xsd:string",
        "readOnly": true
    },
    {
        "@type": "hsd:DataProperty",
        "key": "billingAddress2",
        "name": "Billing Address 2",
        "range": "xsd:string",
        "readOnly": true
    },
    {
        "@type": "hsd:DataProperty",
        "key": "billingCity",
        "name": "Billing City",
        "range": "xsd:string",
        "readOnly": true
    },
    {
        "@type": "hsd:DataProperty",
        "key": "billingState",
        "name": "Billing State",
        "range": "xsd:string",
        "readOnly": true
    },
    {
        "@type": "hsd:DataProperty",
        "key": "billingPostalCode",
        "name": "Billing Postal Code",
        "range": "xsd:string",
        "readOnly": true
    },
    {
        "@type": "hsd:DataProperty",
        "key": "billingCountry",
        "name": "Billing Country",
        "range": "xsd:string",
        "readOnly": true
    },
    {
        "@type": "hsd:DataProperty",
        "key": "deliveryName",
        "name": "Delivery Name",
        "range": "xsd:string"
    },
    {
        "@type": "hsd:DataProperty",
        "key": "shippingAddress1",
        "name": "Delivery Address 1",
        "range": "xsd:string"
    },
    {
        "@type": "hsd:DataProperty",
        "key": "shippingCity",
        "name": "Delivery City",
        "range": "xsd:string"
    },
    {
        "@type": "hsd:DataProperty",
        "key": "shippingAddress2",
        "name": "Delivery Address 2",
        "range": "xsd:string"
    },
    {
        "@type": "hsd:DataProperty",
        "key": "shippingState",
        "name": "Delivery State",
        "range": "xsd:string"
    },
    {
        "@type": "hsd:DataProperty",
        "key": "shippingPostalCode",
        "name": "Delivery Postal Code",
        "range": "xsd:string"
    },
    {
        "@type": "hsd:DataProperty",
        "key": "shippingCountry",
        "name": "Delivery Country",
        "range": "xsd:string"
    },
    {
        "@type": "hsd:DataProperty",
        "key": "isTaxable",
        "name": "Is Taxable",
        "range": "xsd:boolean",
        "default": true
    },
    {
        "@type": "hsd:CoreProperty",
        "key": "externalKey",
        "name": "External Key",
        "range": "xsd:string",
        "readOnly": true
    },
    {
        "@type": "hsd:DataProperty",
        "key": "lastModified",
        "name": "Last Modified",
        "range": "xsd:dateTime",
        "isLastModified": true
    }
]
```
</details>

## <a id="order-item-schema"></a> Appendix SOI - OrderItem JSON Schema  

<details>
  <summary>Click to  show/hide JSON.</summary>

```javascript

[
    {
        "@type": "hsd:RelationProperty",
        "key": "order",
        "name": "Order",
        "range": "Order",
        "format": "link",
        "required": true
    },
    {
        "@type": "hsd:DataProperty",
        "key": "sku",
        "name": "SKU",
        "range": "xsd:string",
        "required": true,
        "format": "smalltext",
        "restrictions": {
            "maxLength" : 15
        }
    },
    {
        "@type": "hsd:DataProperty",
        "key": "lineItemNo",
        "name": "Line Item",
        "required": true,
        "range": "xsd:string",
        "format": "smalltext",
        "restrictions": {
            "maxLength" : 50
        }
    },
    {
        "@type": "hsd:DataProperty",
        "key": "UoM",
        "name": "Unit of Measure",
        "required": true,
        "range": "xsd:string",
        "format": "smalltext",
        "default": "EA",
        "restrictions": {
            "maxLength" : 50
        }
    },
    {
        "@type": "hsd:DataProperty",
        "key": "orderQuantity",
        "name": "Order Quantity",
        "required": true,
        "range": "xsd:integer",
        "format": "number",
        "restrictions": {
            "maxLength" : 10
        }
    }, 
    {
        "@type": "hsd:DataProperty",
        "key": "unitPrice",
        "name": "Unit Price",
        "range": "xsd:float",
        "format": "number",
        "restrictions": {
            "maxLength" : 10
        },
    },
    {
        "@type": "hsd:DataProperty",
        "key": "grossPrice",
        "name": "Gross Price",
        "range": "xsd:float",
        "function": "MULTIPLY('orderQuantity', 'unitPrice')"
        "format": "number",
        "restrictions": {
            "maxLength" : 10
        }
    },
    {
        "@type": "hsd:DataProperty",
        "key": "externalOrderId",
        "name": "External Order ID",
        "range": "xsd:string",
        "format": "text",
        "restrictions": {
            "maxLength" : 25
        }
    },
    {
        "@type": "hsd:DataProperty",
        "key": "externalKey",
        "name": "External Key",
        "range": "xsd:string",
        "format": "text",
        "restrictions": {   
            "maxLength" : 25
        }
    },
    {
        "@type": "hsd:DataProperty",
        "key": "lastModified",
        "name": "Last Modified",
        "range": "xsd:dateTime",
        "isLastModified": true
    }
]
```

</details>

## Appendix SOL - OrderLog JSON Schema <a id="order-log-schema"></a> 


<details>
  <summary>Click to  show/hide JSON.</summary>

```javascript
[
    {
        "@type": "hsd:DataProperty",
        "key": "order",
        "name": "Order",
        "range": "xsd:string",
        "format": "smalltext",
        "readOnly": true
    },
    {
        "@type": "hsd:DataProperty",
        "key": "externalKey",
        "name": "External Key",
        "range": "xsd:string",
        "format": "smalltext",
        "readOnly": true
    },
    {
        "@type": "hsd:DataProperty",
        "key": "errorMessage",
        "name": "Error Message",
        "range": "xsd:string",
        "readOnly": true,
        "format": "plainTextArea",
        "restrictions": {
            "maxLength" : 200
        }
    }
]
```

</details>



## Appendix J - Client Job Description <a id="client-job"></a>

Fetch data from Client ABC API web service which feeds us our **Order** data.

1. HTTP GET listing of new **Order**s from **Client ABC** API. See: [Get 'New' Orders Mock] for an example case.
1. For each new **Order**, HTTP GET the details from the **Client ABC**. See: [Get Order 40721314 Mock] for an example case.
1. Transform the **Order** data, and the **OrderItem**  data into this Eloquent **Order** and **OrderItem** instances and ***seed*** them to the database. See [Database: Seeding](https://laravel.com/docs/7.x/seeding).
1. Any error(s) or warning(s) that should result from this job, are to be saved as a record in the **OrderLog** table.
1. This job should be capable of transform result data from [A], [B], [C] to [X], [Y], [Z]

***Note:*** For each Order, notice the "items" array list these are **Client ABC** order items which we need to convert to our **OrderItem** instances

***Note:*** Take notice the various, different and sometimes odd date formats from **Client ABC** data. Ensure <span style="text-decoration: underline">All Date-Time values are to be stored in UTC in this project.</span>

***Note:*** Schemas and example data should drive the expected value transformations and/or mappings whenever possible.


## Appendix X - Order API <a id="appendix-x"></a> 


### `GET /orders`

```text
GET /orders HTTP/1.1
Host: localhost:8080
```

### `HTTP/1.1 200 OK`

<details>
  <summary>Click to show/hide Response JSON.</summary>

```javascript
[
    {
        "id": 1,
        "key": "5ebb05d0218cc",
        "externalKey": 40721314,
        "retailerCarrierServiceCode": "FDX-Air-Standard Overnight",
        "salesChannel": "EDI",
        "routingInstructions": "FDX-Air-Standard Overnight",
        "customerPONumber": "90994127",
        "accountNumber": "",
        "carrierCode": "FDXSO",
        "partner": "NETSHOPS",
        "shippingAddress1": "1234 Street",
        "shippingAddress2": "3G",
        "shippingCity": "Winterfell",
        "shippingState": "NY",
        "deliveryName": "Jon Snow",
        "shippingCountry": "US",
        "shippingPostalCode": "12345",
        "deliveryLocationCode": "9179215296",
        "billingAddress1": "Walmart Inc.",
        "billingAddress2": "c/o Hayneedle, 2118 Water Ridge Parkway",
        "billingCity": "Charlotte",
        "billingState": "NC",
        "billingCountry": "US",
        "billingPostalCode": "28217",
        "externalReceivedDate": "2020-04-11 11:33:32",
        "shipEndDate": "2019-12-24 20:23:44",
        "shipStartDate": "2019-12-22 20:23:44",
        "orderDate": "2019-12-22 20:23:44",
        "isTaxable": true,
        "dateCreated": "2019-12-22 20:23:44"
        "skus": [
            {
                "id": 1,
                "externalKey": "40721314/0001",
                "orderQuantity": 2,
                "unitPrice": 174.99,
                "grossPrice": 349.98,
                "UoM": "EA",
                "lineItemNo": "0001",
                "order": "5ebb05d0218cc",
                "externalOrderId": 40721314,
                "sku": "41202"
            },
            {
                "id": 2,
                "externalKey": "40721314/0002",
                "orderQuantity": 12,
                "unitPrice": 90.83,
                "grossPrice": 1089.96,
                "UoM": "EA",
                "lineItemNo": "0001",
                "order": "5ebb05d0218cc",
                "externalOrderId": 40721314,
                "sku": "32703"
            }
        ]
    },
    {
        "id": 2,
        "key": "5ebb05d0ecde8",
        "externalKey": 40721325,
        "retailerCarrierServiceCode": "FDX-Air-Express Saver",
        "salesChannel": "EDI",
        "routingInstructions": "FDX-Air-Express Saver",
        "customerPONumber": "91050439",
        "carrierCode": "FDXES",
        "partner": "NETSHOPS",
        "shippingAddress1": "3233 N ARLINGTON HEIGHTS RD",
        "shippingAddress2": "STE 302",
        "shippingCity": "ARLINGTON HEIGHTS",
        "shippingState": "IL",
        "deliveryName": "Ayra Stark",
        "shippingCountry": "US",
        "shippingPostalCode": "60004-1580",
        "deliveryLocationCode": "2243452532",
        "billingAddress1": "Walmart Inc.",
        "billingAddress2": "c/o Hayneedle, 2118 Water Ridge Parkway",
        "billingCity": "Charlotte",
        "billingState": "NC",
        "billingCountry": "US",
        "billingPostalCode": "28217",
        "externalReceivedDate": "2020-04-11 11:33:36",
        "shipEndDate": "2020-01-17 20:23:44",
        "shipStartDate": "2020-01-16 20:23:44",
        "orderDate": "2020-01-16 20:23:44",
        "isTaxable": true,
        "dateCreated": "2020-01-16 20:23:44",
        "skus": [
            {
                "id": 3,
                "externalKey": "40721325/0001",
                "orderQuantity": 2,
                "unitPrice": 97.99,
                "UoM": "EA",
                "lineItemNo": "0001",
                "order": "5ebb05d0ecde8",
                "externalOrderId": 40721325,
                "sku": "65504"
            }
        ]
    } 
]
```

***Note:*** keys in this example were generated with `uniqid()`.

</details>

### `GET /orders?externalKey=40721314`

```text
GET /orders?externalKey=40721314 HTTP/1.1
Host: localhost:8080
```

### `HTTP/1.1 200 OK`

<details>
  <summary>Click to  show/hide JSON.</summary>

```javascript
[{
    "id": 1,
    "key": "5ebb05d0218cc",
    "externalKey": 40721314,
    "retailerCarrierServiceCode": "FDX-Air-Standard Overnight",
    "salesChannel": "EDI",
    "routingInstructions": "FDX-Air-Standard Overnight",
    "customerPONumber": "90994127",
    "accountNumber": "",
    "carrierCode": "FDXSO",
    "partner": "NETSHOPS",
    "shippingAddress1": "12345 Street",
    "shippingAddress2": "3G",
    "shippingCity": "Winterfell",
    "shippingState": "NY",
    "deliveryName": "Jon Snow",
    "shippingCountry": "US",
    "shippingPostalCode": "12345",
    "deliveryLocationCode": "9179215296",
    "billingAddress1": "Walmart Inc.",
    "billingAddress2": "c/o Hayneedle, 2118 Water Ridge Parkway",
    "billingCity": "Charlotte",
    "billingState": "NC",
    "billingCountry": "US",
    "billingPostalCode": "28217",
    "externalReceivedDate": "2020-04-11 11:33:32",
    "shipEndDate": "2019-12-24 20:23:44",
    "shipStartDate": "2019-12-22 20:23:44",
    "orderDate": "2019-12-22 20:23:44",
    "isTaxable": true,
    "dateCreated": "2019-12-22 20:23:44",
    "skus": [
        {
            "id": 1,
            "externalKey": "40721314/0001",
            "orderQuantity": 2,
            "unitPrice": 174.99,
            "grossPrice": 349.98,
            "UoM": "EA",
            "lineItemNo": "0001",
            "order": "5ebb05d0218cc",
            "externalOrderId": 40721314,
            "sku": "41202"
        },
        {
            "id": 2,
            "externalKey": "40721314/0002",
            "orderQuantity": 12,
            "unitPrice": 90.83,
            "grossPrice": 1089.96,
            "UoM": "EA",
            "lineItemNo": "0001",
            "order": "5ebb05d0218cc",
            "externalOrderId": 40721314,
            "sku": "32703"
        }
    ]
}]
```

***Note:*** keys in this example were generated with `uniqid()`.

</details>


### `GET /orders?externalKey=40721325`

```text
GET /orders?externalKey=40721325 HTTP/1.1
Host: localhost:8080
```

### `HTTP/1.1 200 OK`

<details>
  <summary>Click to  show/hide JSON.</summary>

```javascript
[{
    "id": 1,
    "key": "5ebb05d0ecde8",
    "externalKey": 40721325,
    "retailerCarrierServiceCode": "FDX-Air-Express Saver",
    "salesChannel": "EDI",
    "routingInstructions": "FDX-Air-Express Saver",
    "customerPONumber": "91050439",
    "accountNumber": "",
    "carrierCode": "FDXES",
    "partner": "NETSHOPS",
    "shippingAddress1": "3233 N ARLINGTON HEIGHTS RD",
    "shippingAddress2": "STE 302",
    "shippingCity": "ARLINGTON HEIGHTS",
    "shippingState": "IL",
    "deliveryName": "Ayra Stark",
    "shippingCountry": "US",
    "shippingPostalCode": "60004-1580",
    "deliveryLocationCode": "2243452532",
    "billingAddress1": "Walmart Inc.",
    "billingAddress2": "c/o Hayneedle, 2118 Water Ridge Parkway",
    "billingCity": "Charlotte",
    "billingState": "NC",
    "billingCountry": "US",
    "billingPostalCode": "28217",
    "externalReceivedDate": "2020-04-11 11:33:36",
    "shipEndDate": "2020-01-17 20:23:44",
    "shipStartDate": "2020-01-16 20:23:44",
    "orderDate": "2020-01-16 20:23:44",
    "isTaxable": true,
    "dateCreated": "2020-01-16 20:23:44",
    "skus": [
        {
            "id": 3,
            "externalKey": "40721325/0001",
            "orderQuantity": 2,
            "unitPrice": 97.99,
            "UoM": "EA",
            "lineItemNo": "0001",
            "order": "5ebb05d0ecde8",
            "externalOrderId": 40721325,
            "sku": "65504"
        }
    ]
}]
```

***Note:*** keys in this example were generated with `uniqid()`.

</details>


## Appendix Y - OrderItem API <a id="appendix-y"></a> 

### `GET /orderItems?order=40721314`

```text
GET /orderItems?order=40721314 HTTP/1.1
Host: localhost:8080
```

### `HTTP/1.1 200 OK`

<details>
  <summary>Click to show/hide JSON Response</summary>

```javascript
[
    {
        "id": 1,
        "externalKey": "40721314/0001",
        "orderQuantity": 2,
        "unitPrice": 174.99,
        "grossPrice": 349.98,
        "UoM": "EA",
        "lineItemNo": "0001",
        "order": "5ebb05d0218cc",
        "externalOrderId": 40721314,
        "sku": "41202"
    },
    {
        "id": 2,
        "externalKey": "40721314/0002",
        "orderQuantity": 12,
        "unitPrice": 90.83,
        "grossPrice": 1089.96,
        "UoM": "EA",
        "lineItemNo": "0001",
        "order": "5ebb05d0218cc",
        "externalOrderId": 40721314,
        "sku": "32703"
    }
]
```
</details>


### `GET /orderItems?order=40721325`
```text
GET /orderItems?order=40721325 HTTP/1.1
Host: localhost:8080
```

### `HTTP/1.1 200 OK`

<details>
  <summary>Click to show/hide JSON.</summary>

```javascript
[
    {
        "id": 3,
        "externalKey": "40721325/0001",
        "orderQuantity": 2,
        "unitPrice": 97.99,
        "UoM": "EA",
        "lineItemNo": "0001",
        "order": "5ebb05d0ecde8",
        "externalOrderId": 40721325,
        "sku": "65504"
    }
]
```
</details>


## Appendix Z - OrderLog API

### `GET /order-logs`
```text
GET /order-logs HTTP/1.1
Host: localhost:8080
```
### `HTTP/1.1 200 OK`

<details>
  <summary>Click to show/hide JSON Response</summary>

```javascript
[
    {
        "order": "5ebb05d0ecde8",
        "externalKey": "40721325",
        "message": "'Customer' is a required field for Order and not provided."
    },
    {
        "order": "5ebb05d0218cc",
        "externalKey": "40721314",
        "message": "'SKU' is a required field for OrderItem and was not provided."
    },
    {
        "order": "5ebb05d0218cc",
        "externalKey": "40721314",
        "message": "Truncated incorrect INTEGER value: 5.5 on COLUMN orderQuantity"
    }
]
```
</details>